import {Film} from "./film.model";

export class FilmResponse {
  page: number;
  results: Film[];
  total_pages: number;
  total_results: number;
}
