import {Genre} from "./genre.model";

export class GenreResponse {
  genres: Genre[];
}
