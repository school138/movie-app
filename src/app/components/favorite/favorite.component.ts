import { Component, OnInit } from '@angular/core';
import {Film} from '../../models/film.model';
import{FavoriteFilmService} from "../../services/favorite-film.service";
import {FilmResponse} from "../../models/filmResponse.model";

@Component({
  selector: 'app-favorite',
  templateUrl: './favorite.component.html',
  styleUrls: ['./favorite.component.css']
})
export class FavoriteComponent implements OnInit {

  favoriteFilms: Film[] = [] ;

  constructor(private favoriteFilmService: FavoriteFilmService) { }



  ngOnInit(): void {
    this.navbarShrink();
    this.getFavoritesList();
  }

  navbarShrink () {
    const navbarCollapsible = document.body.querySelector('#mainNav');
    if (!navbarCollapsible) {
      return;
    }
    navbarCollapsible.classList.add('navbar-shrink');
  };

  getFavoritesList() {
    this.favoriteFilmService.getFavorites().subscribe((data: Array<Film>) => this.favoriteFilms = data);
  }


}
