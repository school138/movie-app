import { Component, OnInit } from '@angular/core';
import {FilmService} from "../../services/film.service";
import {Film} from "../../models/film.model";
import {FilmResponse} from "../../models/filmResponse.model";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  films: Film[] = [] ;
  searchInput: any;

  constructor(private filmService: FilmService) { }

  ngOnInit(): void {
    this.navbarShrink();
    this.filmService.getCurrentFilms().subscribe((data: FilmResponse)  => this.films = data.results);
  }

  navbarShrink () {
    const navbarCollapsible = document.body.querySelector('#mainNav');
    if (!navbarCollapsible) {
      return;
    }
    navbarCollapsible.classList.add('navbar-shrink');
  };

  searchFilms() {
    this.filmService.searchFilms(this.searchInput).subscribe((data: FilmResponse)  => this.films = data.results);
  }
}
