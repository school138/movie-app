import { Component, OnInit } from '@angular/core';
import {FilmService} from "../../services/film.service";
import {GenreResponse} from "../../models/genreResponse.model";
import {Genre} from "../../models/genre.model";
import {FilmResponse} from "../../models/filmResponse.model";
import {Film} from "../../models/film.model";

@Component({
  selector: 'app-genre',
  templateUrl: './genre.component.html',
  styleUrls: ['./genre.component.css']
})
export class GenreComponent implements OnInit {

  genres: Genre[] = [] ;
  selectedGenres: Genre[] ;

  films: Film[] = [] ;

  constructor(private filmService: FilmService) { }

  ngOnInit(): void {
    this.navbarShrink();
    this.filmService.getGenres().subscribe((data: GenreResponse)  => this.genres = data.genres);
  }

  navbarShrink () {
    const navbarCollapsible = document.body.querySelector('#mainNav');
    if (!navbarCollapsible) {
      return;
    }
    navbarCollapsible.classList.add('navbar-shrink');
  };

  search() {
    let genresSearchString = this.selectedGenres.map(x => x.id).toString();
    this.filmService.getFilmsByGenres(genresSearchString).subscribe((data: FilmResponse)  => this.films = data.results);
  }
}
