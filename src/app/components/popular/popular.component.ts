import { Component, OnInit } from '@angular/core';
import {Film} from "../../models/film.model";
import {FilmService} from "../../services/film.service";
import {FilmResponse} from "../../models/filmResponse.model";

@Component({
  selector: 'app-popular',
  templateUrl: './popular.component.html',
  styleUrls: ['./popular.component.css']
})
export class PopularComponent implements OnInit {

  films: Film[] = [] ;

  constructor(private filmService: FilmService) { }

  ngOnInit(): void {
    this.navbarShrink();
    this.filmService.getPopularFilms().subscribe((data: FilmResponse)  => this.films = data.results);
  }

  navbarShrink () {
    const navbarCollapsible = document.body.querySelector('#mainNav');
    if (!navbarCollapsible) {
      return;
    }
    navbarCollapsible.classList.add('navbar-shrink');
  };

}
