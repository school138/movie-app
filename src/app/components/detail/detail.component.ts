import { Component, OnInit } from '@angular/core';
import {FilmService} from "../../services/film.service";
import {Film} from "../../models/film.model";
import {ActivatedRoute, Router} from "@angular/router";
import {FavoriteFilmService} from "../../services/favorite-film.service";
import {concat, Observable, take} from "rxjs";

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  film: Film;
  id: string;
  isFav: boolean;
  isFavorites$: Observable<boolean>;

  constructor(private filmService: FilmService,
              private favoriteFilmService: FavoriteFilmService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit(): void {
    this.navbarShrink();
    this.id = this.route.snapshot.params['id'];
    this.filmService.getFilm(this.id).subscribe((data: Film)  => this.film = data);
    this.isFavorites$ = this.favoriteFilmService.isFavorite(this.id);
  }

  navbarShrink () {
    const navbarCollapsible = document.body.querySelector('#mainNav');
    if (!navbarCollapsible) {
      return;
    }
    navbarCollapsible.classList.add('navbar-shrink');
  };

  removeFromFavorite() {
    var one =   this.favoriteFilmService.deleteFavorite(parseInt(this.id)).pipe(take(1));
    var two = this.favoriteFilmService.isFavorite(this.id).pipe(take(1));
    concat(one, two).subscribe(x => {
      this.isFavorites$ = two;
    });
  }

  addToFavorite() {
    var one =   this.favoriteFilmService.insertFavorite(this.film).pipe(take(1));
    var two = this.favoriteFilmService.isFavorite(this.id).pipe(take(1));
    concat(one, two).subscribe(x => {
      this.isFavorites$ = two;
    });
  }
}
