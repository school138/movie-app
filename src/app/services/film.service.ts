import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {FilmResponse} from "../models/filmResponse.model";

@Injectable({
  providedIn: 'root'
})
export class FilmService {

  baseUrl: String = 'https://api.themoviedb.org/3/';
  apiKey: String = '064ccace9ee00e147e15fce0d3fdd733';
  urlAddition: String = '&language=de-DE&region=DE';

  constructor(private http:HttpClient) { }


  getPopularFilms(): Observable<any> {
    let url = this.baseUrl + 'movie/popular?api_key=' + this.apiKey + this.urlAddition;
    return this.http.get(url);
  }

  getCurrentFilms(): Observable<FilmResponse> {
    let url = this.baseUrl + 'movie/now_playing?api_key=' + this.apiKey + this.urlAddition;
    return this.http.get<FilmResponse>(url);
  }

  getBestFilms(): Observable<any> {
    let url = this.baseUrl + 'movie/top_rated?api_key=' + this.apiKey + this.urlAddition;
    return this.http.get(url);
  }

  searchFilms(searchQuery: string): Observable<any> {
    let url = this.baseUrl + 'search/movie?api_key=' + this.apiKey + '&query=' + searchQuery;
    return this.http.get(url);
  }

  getFilmsByGenres(genres: string): Observable<any> {
    let url = this.baseUrl + 'discover/movie?api_key=' + this.apiKey + '&with_genres=' + genres;
    return this.http.get(url);
  }


  getGenres(): Observable<any> {
    let url = this.baseUrl + 'genre/movie/list?api_key=' + this.apiKey + this.urlAddition;
    return this.http.get(url);
  }

  getFilm(id: string): Observable<any> {
    //return this.http.get(`${this.baseUrl}movie/${id}?api_key=${this.apiKey}`);
    let url = this.baseUrl + 'movie/' + id +'?api_key=' + this.apiKey + this.urlAddition;
    console.log(url);
    return this.http.get(url);
  }


}
