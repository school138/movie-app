import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Film} from '../models/film.model';

@Injectable({
  providedIn: 'root'
})
export class FavoriteFilmService {

  constructor(private http: HttpClient) { }

  private baseUrl = 'http://localhost:3000';

  public getFavorites(): Observable<Array<Film>> {
    return this.http.get<Array<Film>>(this.baseUrl + '/favorites');
  }

  public insertFavorite(film: Film): Observable<any> {
    return this.http.post(this.baseUrl + '/favorite', film);
  }

  public deleteFavorite(id: number): Observable<any> {
    const url = this.baseUrl + '/favorite/' + id;
    return this.http.delete(url);
  }

  isFavorite(id: string): Observable<any> {
    const url = this.baseUrl + '/isFavorite/' + id;
    return this.http.get(url);
  }
}
