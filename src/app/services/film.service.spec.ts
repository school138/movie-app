import { TestBed } from '@angular/core/testing';

import { FilmService } from './film.service';
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {FilmResponse} from "../models/filmResponse.model";
import {GenreResponse} from "../models/genreResponse.model";

describe('FilmService', () => {
  let service: FilmService;
  let controller: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [FilmService],
    });
    service = TestBed.inject(FilmService);
    controller = TestBed.inject(HttpTestingController);
  });

  it('should get the current popular films and have FilmResponse as the type', () => {
    service.getPopularFilms().subscribe((data: FilmResponse)  => {
      expect(data).toEqual(jasmine.any(GenreResponse));
      }
    );
  });

  it('should get the best films and have FilmResponse as the type', () => {
    service.getBestFilms().subscribe((data: FilmResponse)  => {
        expect(data).toEqual(jasmine.any(FilmResponse));
      }
    );
  });

  it('should get the current running films and have FilmResponse as the type', () => {
    service.getCurrentFilms().subscribe((data: FilmResponse)  => {
        expect(data).toEqual(jasmine.any(FilmResponse));
      }
    );
  });

  it('should get the genres and have GenreResponse as the type', () => {
    service.getGenres().subscribe((data)  => {
        expect(data).toEqual(jasmine.any(GenreResponse));
      }
    );
  });


});
