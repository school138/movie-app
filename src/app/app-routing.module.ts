import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "./components/home/home.component";
import {PopularComponent} from "./components/popular/popular.component";
import {BestComponent} from "./components/best/best.component";
import {FavoriteComponent} from "./components/favorite/favorite.component";
import {PageNotFoundComponent} from "./components/page-not-found/page-not-found.component";
import {LandingPageComponent} from "./components/landing-page/landing-page.component";
import {GenreComponent} from "./components/genre/genre.component";
import {DetailComponent} from "./components/detail/detail.component";

const routes: Routes = [
  { path: 'start', component: LandingPageComponent },
  { path: 'home', component: HomeComponent },
  { path: 'popular', component: PopularComponent },
  { path: 'best', component: BestComponent },
  { path: 'genre', component: GenreComponent },
  { path: 'favorite', component: FavoriteComponent },
  { path: 'favorite/:id', component: FavoriteComponent },
  { path: 'detail/:id', component: DetailComponent },
  { path: '', redirectTo: '/start', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
